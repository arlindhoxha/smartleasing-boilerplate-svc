package org.rbko.rbi.smartleasing.boilerplateservice.common.enums;

public enum Message {
    SAMPLE_NOT_FOUND_BY_ID("sample.error.sample.not.found.by.id"),
    SOMETHING_WENT_WRONG("system.error.something.went.wrong"),
    SOMETHING_WENT_WRONG_CONTACT_ADMINISTRATOR("system.error.something.went.wrong.contact.your.administrator"),
    VALIDATION_EXCEPTION("error.dto.validation.exception");

    private String key;

    Message(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
