package org.rbko.rbi.smartleasing.boilerplateservice.controller.v1;

import org.rbko.rbi.smartleasing.boilerplateservice.common.enums.Message;
import org.rbko.rbi.smartleasing.boilerplateservice.dto.SampleDto;
import org.rbko.rbi.smartleasing.boilerplateservice.exception.SampleNotFoundException;
import org.rbko.rbi.smartleasing.boilerplateservice.service.SampleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(
        path = "/api/v1/sample",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class SampleController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private final SampleService sampleService;

    public SampleController(SampleService sampleService, MessageSource messageSource) {
        this.sampleService = sampleService;
    }


    /**
     * Send header: "Locale": "bg" to determine language.
     * @param sampleId
     * @return
     */
    @GetMapping
    public ResponseEntity<List<SampleDto>> getListOfSamples() {
        List<SampleDto> listOfSamples = sampleService.getListOfSamples();

        return new ResponseEntity<>(listOfSamples, HttpStatus.OK);
    }

    /**
     * Create a sample.
     *
     * @param sampleDto - sample data.
     * @return Created sample data, or throws exception otherwise.
     */
    @PostMapping
    public ResponseEntity<SampleDto> createSample(@Valid @RequestBody SampleDto sampleDto) {
        SampleDto createdSample = sampleService.createSample(sampleDto);
        return new ResponseEntity<>(createdSample, HttpStatus.CREATED);
    }

    /**
     * Send header: "Locale": "bg" to determine language.
     * @param sampleId
     * @return
     */
    @GetMapping("/{sampleId}")
    public ResponseEntity<SampleDto> getSample(@PathVariable Integer sampleId) {
        SampleDto sampleDto = sampleService.getSample(sampleId);

        if (sampleDto == null) {
          throw new SampleNotFoundException(Message.SAMPLE_NOT_FOUND_BY_ID, sampleId);
        }

        return new ResponseEntity<>(sampleDto, HttpStatus.OK);
    }
}
