package org.rbko.rbi.smartleasing.boilerplateservice.event;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class Publisher {

  private KafkaTemplate<String, Object> kafkaTemplate;

  public Publisher(KafkaTemplate<String, Object> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  /**
   * @param producerRecord - key represents event key, while body represents event payload.
   */
  public void publishEvent(ProducerRecord<String, Object> producerRecord) {
    kafkaTemplate.send(producerRecord);
  }
}
