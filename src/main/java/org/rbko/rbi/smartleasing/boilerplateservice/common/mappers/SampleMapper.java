package org.rbko.rbi.smartleasing.boilerplateservice.common.mappers;

import org.modelmapper.Condition;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.rbko.rbi.smartleasing.boilerplateservice.dto.SampleDto;
import org.rbko.rbi.smartleasing.boilerplateservice.model.Sample;

import java.util.List;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Notice: Postfix V1,V2,... is used only to demonstrate different approaches for
 * the same problem or result. Please do not use version postfix as naming convention
 * for this purpose.
 */
public class SampleMapper {
    private static Converter<String, String> toUppercase = ctx -> ctx.getSource() == null ?
            null :
            ctx.getSource().toUpperCase();
    private static Condition notNull = ctx -> ctx.getSource() != null;

    /**
     * Implicit.
     * Maps every field possible to Sample class. Or the case of flattening
     * the objects.Type safe conversion.
     *
     * Ex:
     * class Address { String street; }
     * class Order { Address shippingAddress; }
     *
     * class OrderDTO { shippingStreetAddress; }
     */
    public static Sample toSampleV1(SampleDto sampleDto) {
        ModelMapper modelMapper = new ModelMapper();
//        modelMapper.typeMap(SampleDto.class, Sample.class).addMappings(mapper -> {
//            mapper.skip(Sample::setUniqueNameAndImmutable);
//        });
        // If we need to make sure that all destination properties are matched then we
        // use validate() method. If didn't match all properties then Validation exception
        // is thrown.
        // modelMapper.validate();

        return modelMapper.map(sampleDto, Sample.class);
    }

    /**
     * Maps automatically SampleDto to Sample.
     *
     * @param sampleDtoList list of SampleDto that will be mapped.
     * @return List of mapped Samples.
     */
    public static List<Sample> toSampleList(Collection<SampleDto> sampleDtoList) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.typeMap(SampleDto.class, Sample.class).addMappings(mapper -> {
            mapper.skip(Sample::setUniqueNameAndImmutable);
        });

        return sampleDtoList.stream()
                .map(entity -> modelMapper.map(entity, Sample.class))
                .collect(Collectors.toList());
    }

    /**
     * Map field by field for more complex objects. Projection method. Type safe conversion.
     * PropertyMap.
     * Ex:
     * public class Order { Address address; }
     *
     * public class Address {
     *   private String street;
     *   private String city;
     * }
     *
     *
     */
    public static Sample toSampleV2(SampleDto sampleDto) {
        ModelMapper mapper = new ModelMapper();
        PropertyMap<Sample, SampleDto> orderMap = new PropertyMap<>() {
            protected void configure() {
                map().setCreatedOn(source.getCreatedOn());
                map().setId(source.getId());
            }
        };
        mapper.addMappings(orderMap);

        return mapper.map(sampleDto, Sample.class);
    }

    /**
     * Explicit mapping. Deep mapping. Use of converter.
     */
    public static Sample toSampleV3(SampleDto sampleDto) {
        ModelMapper modelMapper = new ModelMapper();

        modelMapper.typeMap(SampleDto.class, Sample.class).addMappings(mapper -> {
            mapper.map(src -> src.getCreatedOn(), Sample::setCreatedOn);
            mapper.using(toUppercase).map(src -> src.getCreatedByName(), Sample::setCreatedByName);
            mapper.map(SampleDto::getId, Sample::setId);
        });

        return modelMapper.map(sampleDto, Sample.class);
    }

    // Model to DTO

    /**
     * Implicit.
     */
    public static SampleDto toSampleDtoV1(Sample sample) {
        ModelMapper modelMapper = new ModelMapper();

        return modelMapper.map(sample, SampleDto.class);
    }

    /**
     * Maps automatically SampleDto to Sample.
     *
     * @param sampleList list of Sample that will be mapped.
     * @return List of mapped SampleDto-s.
     */
    public static List<SampleDto> toSampleDtoList(Collection<Sample> sampleList) {
        ModelMapper modelMapper = new ModelMapper();
//        modelMapper.typeMap(SampleDto.class, Sample.class).addMappings(mapper -> {
//            mapper.skip(Sample::setUniqueNameAndImmutable);
//        });

        return sampleList.stream()
                .map(entity -> modelMapper.map(entity, SampleDto.class))
                .collect(Collectors.toList());
    }

}
