package org.rbko.rbi.smartleasing.boilerplateservice.event;

/**
 * Event enum
**/
public enum Events {
  SAMPLE_UPDATED("sample.updated");

  private String key;

  Events(String key) {
    this.key = key;
  }

  public String getKey() {
    return key;
  }
}
