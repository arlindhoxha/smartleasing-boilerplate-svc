package org.rbko.rbi.smartleasing.boilerplateservice.service;

import org.rbko.rbi.smartleasing.boilerplateservice.dto.SampleDto;
import org.rbko.rbi.smartleasing.boilerplateservice.model.Sample;
import org.rbko.rbi.smartleasing.boilerplateservice.repository.SampleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.rbko.rbi.smartleasing.boilerplateservice.common.mappers.SampleMapper.*;

@Service
public class SampleServiceImp implements SampleService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private final SampleRepository sampleRepository;

    public SampleServiceImp(SampleRepository sampleRepository) {
        this.sampleRepository = sampleRepository;
    }

    @Override
    public List<SampleDto> getListOfSamples() {
        logger.info("Retrieving list of sample data...");
        List<Sample> listOfSamples = sampleRepository.findAll();

        return toSampleDtoList(listOfSamples);
    }

    @Override
    public SampleDto createSample(SampleDto sampleDto) {
        logger.info("Creating new sample data...");
        Sample sample = toSampleV1(sampleDto);
        Sample sampleResult = sampleRepository.save(sample);

        return toSampleDtoV1(sampleResult);
    }

    @Override
    public SampleDto getSample(Integer sampleId) {
        logger.info("Get sample data by identification number...");
        Sample sample = new Sample();
        sample.setId(sampleId);

        return toSampleDtoV1(sample);
    }
}
