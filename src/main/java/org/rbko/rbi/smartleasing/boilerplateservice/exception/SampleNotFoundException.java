package org.rbko.rbi.smartleasing.boilerplateservice.exception;

import org.rbko.rbi.smartleasing.boilerplateservice.common.enums.Message;
import org.rbko.rbi.smartleasing.boilerplateservice.common.configurations.Translator;

public class SampleNotFoundException extends RuntimeException {
    public SampleNotFoundException(String messageKey) {
        super(Translator.toLocale(messageKey, null));
    }

    public SampleNotFoundException(Message messageKey) {
        super(Translator.toLocale(messageKey.getKey(), null));
    }

    public SampleNotFoundException(String messageKey, Integer sampleId) {
        super(Translator.toLocale(messageKey, new Object[] { sampleId.toString() }));
    }

    public SampleNotFoundException(Message messageKey, Integer sampleId) {
        super(Translator.toLocale(messageKey.getKey(), new Object[] { sampleId.toString() }));
    }
}
