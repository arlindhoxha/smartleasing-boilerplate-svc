package org.rbko.rbi.smartleasing.boilerplateservice.event.conumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.rbko.rbi.smartleasing.boilerplateservice.dto.SampleDto;
import org.rbko.rbi.smartleasing.boilerplateservice.event.Events;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class SampleEventConsumer {

  private static final String TOPIC = "sample-topic";
  private ObjectMapper objectMapper = new ObjectMapper();

  /**
   * @param payload - key represents event ky while value represent event payload. On each event
   *                published to specified topic this method is triggered.
   *                Event handling is propagated based on event key
   */
  @KafkaListener(topics = TOPIC, groupId = "sample-event-group")
  public void listen(ConsumerRecord<String, Object> payload) {
    String eventKey = payload.key();
    if(eventKey.equals(Events.SAMPLE_UPDATED.getKey())) {
      this.onSampleDtoUpdated(payload.value());
    }
  }

  /**
   * Handler method for sample.updated event key. Event payload is parsed to specific dto then
   * we call our pre-defined method on sample service.
   * @param data - Event payload
   *
   */
  private void onSampleDtoUpdated(Object data) {
    try {
      SampleDto updatedSampleDto = objectMapper
          .readValue(objectMapper.writeValueAsString(data), SampleDto.class);

      // TODO: process handled event
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }
}
