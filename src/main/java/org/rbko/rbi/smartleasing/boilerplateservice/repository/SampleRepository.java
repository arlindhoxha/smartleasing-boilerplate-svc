package org.rbko.rbi.smartleasing.boilerplateservice.repository;

import org.rbko.rbi.smartleasing.boilerplateservice.model.Sample;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SampleRepository extends JpaRepository<Sample, Long> {

    /**
     * Custom method. Find by FieldOne type String and FieldTwo type String.
     */
    Sample findByIdAndCreatedOn(String fieldOneValue, String fieldTwoValue);
//
//    /**
//     * Custom query method using JPQL.
//     */
//    @Query("SELECT sample FROM Sample sample WHERE sample.createdByName = :createdBy and sample.isActive = :isActive")
//    Sample findAllSamplesByCreatedByAndIsActive(
//            @Param("createdBy") Integer status,
//            @Param("isActive") Boolean active, Pageable pageable);
//
//    /**
//     * Insert is not a part of JPA.
//     * @param name
//     * @param age
//     */
//    @Modifying
//    @Query(value ="INSERT INTO Sample (createdByName, createdOn) values (:createdByName, :createdOn)",
//            nativeQuery = true)
//    void insertSampleSQL(@Param("createdByName") String name, @Param("createdOn") Integer age);
}
