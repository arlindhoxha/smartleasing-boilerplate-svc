package org.rbko.rbi.smartleasing.boilerplateservice.event.publisher;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.rbko.rbi.smartleasing.boilerplateservice.dto.SampleDto;
import org.rbko.rbi.smartleasing.boilerplateservice.event.Events;
import org.rbko.rbi.smartleasing.boilerplateservice.event.Publisher;
import org.rbko.rbi.smartleasing.boilerplateservice.event.publisher.SampleEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class SampleEventPublisherImpl implements SampleEventPublisher {

  private static final String TOPIC = "sample-topic";
  private Publisher publisher;

  public SampleEventPublisherImpl(Publisher publisher) {
    this.publisher = publisher;
  }

  /**
   * Publish sample updated event.
   * @param sampleDto - Event payload
   */
  @Override
  public void publishSampleUpdated(SampleDto sampleDto) {
    this.publisher.publishEvent(new ProducerRecord<>(TOPIC, Events.SAMPLE_UPDATED.getKey(), sampleDto));
  }
}
