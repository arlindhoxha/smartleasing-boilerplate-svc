package org.rbko.rbi.smartleasing.boilerplateservice.common.configurations;

import com.sun.istack.Nullable;
import org.rbko.rbi.smartleasing.boilerplateservice.common.enums.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class Translator {
    private static MessageSource messageSource;

    public static String toLocale(String messageKey, @Nullable Object[] args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(messageKey, args, locale);
    }

    public static String toLocale(Message messageKey, @Nullable Object[] args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(messageKey.getKey(), args, locale);
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        Translator.messageSource = messageSource;
    }
}
