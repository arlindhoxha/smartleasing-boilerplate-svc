package org.rbko.rbi.smartleasing.boilerplateservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SampleDto {

    /**
     * Sample identification.
     */
//    @NotNull(message = "{sample.dto.error.identification.not.empty}")
    private Integer id;

    /**
     * Sample unique name.
     */
    @NotEmpty(message = "{sample.dto.error.unique.name.not.empty}")
    private String uniqueNameAndImmutable;

    /**
     * Date of sample created on.
     */
    private Date createdOn;

    /**
     * Name of the author.
     */
    private String createdByName;
}
