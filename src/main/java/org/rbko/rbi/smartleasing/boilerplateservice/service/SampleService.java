package org.rbko.rbi.smartleasing.boilerplateservice.service;

import org.rbko.rbi.smartleasing.boilerplateservice.dto.SampleDto;

import java.util.List;

public interface SampleService {

    /**
     * Get the list of all samples.
     *
     * @return List of Samples.
     */
    List<SampleDto> getListOfSamples();

    /**
     * Create new sample.
     *
     * @return Created sample data, or error otherwise.
     */
    SampleDto createSample(SampleDto sampleDto);

    /**
     * Get sample data by identification number.
     *
     * @param sampleId Sample identification number.
     * @return Sample data, or null otherwise.
     */
    SampleDto getSample(Integer sampleId);

}
