package org.rbko.rbi.smartleasing.boilerplateservice.event.publisher;

import org.rbko.rbi.smartleasing.boilerplateservice.dto.SampleDto;

public interface SampleEventPublisher {

  void publishSampleUpdated(SampleDto sampleDto);
}
