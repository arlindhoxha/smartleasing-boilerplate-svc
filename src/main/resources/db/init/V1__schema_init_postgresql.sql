/* Database init */
-- CREATE DATABASE IF NOT EXISTS smartleasing_boilerplate_svc;
-- USE smartleasing_boilerplate_svc;

/* Table definition: Samples */
-- DROP TABLE IF EXISTS samples;
CREATE TABLE samples (
    id                          SERIAL CONSTRAINT unique_create_by_name_samples PRIMARY KEY,
    unique_name_and_immutable   VARCHAR(255),
    created_on                  DATE DEFAULT CURRENT_DATE,
    created_by_name             VARCHAR(255),
--     updated_on                  DATE,
--     updated_by_name             VARCHAR(255),
--     decimal_field               DECIMAL(12,2),
    is_sample_active            BOOLEAN
);

-- ALTER TABLE samples LATE ADD CONSTRAINT pk_id_samples SERIAL PRIMARY KEY (id);
ALTER TABLE samples ADD CONSTRAINT unique_create_by_name_samples UNIQUE (created_by_name);
ALTER TABLE samples ADD CONSTRAINT created_by_name_check_min_max_samples
                    CHECK (LENGTH(created_by_name) >= 2 AND LENGTH(created_by_name) <= 255);
/* Table definition: Samples - End */