FROM docker.rbinternational.corp/merlin-docker-base/merlin-docker-build-gradle:1.0.RELEASE as BUILD_IMAGE
FROM docker.rbinternational.corp/merlin-docker-base/merlin-docker-base-java:13.0.RELEASE

COPY --from=BUILD_IMAGE  /opt/build/build/dependencies /service/dependencies
COPY --from=BUILD_IMAGE  /opt/build/build/libs/*.jar /service/

ENV MAIN_CLASS org.rbko.rbi.smartleasing.boilerplateservice.BoilerplateServiceApplication

EXPOSE 8080